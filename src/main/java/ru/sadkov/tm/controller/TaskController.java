package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.sadkov.tm.dto.TaskDTO;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskController {

    @NotNull
    @Autowired
    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;

    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.GET)
    public List<TaskDTO> tasks(@PathVariable(name = "id") @NotNull final String projectId){
        @NotNull final List<Task> taskList = taskService.findAllByProjectId(projectId);
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        for (@NotNull final Task p:taskList) {
            tasks.add(taskService.convert(p));
        }
        return tasks;
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public List<TaskDTO> allTasks(){
        @NotNull final List<Task> taskList = taskService.findAll();
        System.out.println(taskList.size());

        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        for (@NotNull final Task p:taskList) {
            tasks.add(taskService.convert(p));
        }
        return tasks;
    }

    @RequestMapping(value = "/tasks/task/{id}", method = RequestMethod.GET)
    public TaskDTO getTask(@PathVariable(name = "id") @NotNull final String id){
        return taskService.convert(taskService.findOneById(id));
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
   public TaskDTO createTask(@RequestBody @NotNull final TaskDTO taskDTO){
        taskService.saveTask(taskDTO.getName(),projectService.findOne(taskDTO.getProjectId()).getName(),taskDTO.getDescription());
        return taskDTO;
    }

    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.PUT)
    public void updateTask(@PathVariable(name = "id") @NotNull final String id,
                           @RequestBody @NotNull final TaskDTO taskDTO){
        taskService.update(id,taskDTO.getName(),taskDTO.getDescription());
    }

    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.DELETE)
    public void deleteTask(@PathVariable(name = "id") @NotNull final String id){
        taskService.removeById(id);
    }
}
