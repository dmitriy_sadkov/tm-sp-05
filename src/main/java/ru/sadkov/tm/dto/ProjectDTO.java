package ru.sadkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.enumerate.Status;


import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "projects")
public class ProjectDTO implements Serializable {

    @NotNull
    String id;

    @NotNull
    String name;

    @Nullable
    String description;

    @NotNull
    String userId;

    @NotNull
    Date dateCreate;

    @Nullable
    Date dateBegin;

    @Nullable
    Date dateEnd;

    @NotNull
    Status status;
}
